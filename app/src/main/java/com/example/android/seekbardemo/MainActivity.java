package com.example.android.seekbardemo;

import android.content.Context;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    private SeekBar mMediaVolume,mAlarmVolume,mNotificationVolume,mRingerVolume;
    private AudioManager audioManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.setVolumeControlStream(AudioManager.STREAM_RING);
        this.setVolumeControlStream(AudioManager.STREAM_ALARM);
        this.setVolumeControlStream(AudioManager.STREAM_NOTIFICATION);
        initControls();
    }
    private void initControls() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mMediaVolume =  findViewById(R.id.volume_seekBar);

        mMediaVolume.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mMediaVolume.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));

        mRingerVolume = findViewById(R.id.ringer_volume_seekBar);
        mRingerVolume.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_RING));
        mRingerVolume.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_RING));
        mAlarmVolume =findViewById(R.id.alarm_volume_seekbar);
        mAlarmVolume.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_ALARM));
        mAlarmVolume.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_ALARM));

        mNotificationVolume = findViewById(R.id.notification_volume_seekbar);
        mNotificationVolume.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION));
        mNotificationVolume.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_NOTIFICATION));

        try {

            mMediaVolume
                    .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        public void onStopTrackingTouch(SeekBar arg0) {
                        }

                        public void onStartTrackingTouch(SeekBar arg0) {
                        }

                        public void onProgressChanged(SeekBar arg0,
                                                      int progress, boolean arg2) {
                            audioManager.setStreamVolume(
                                    AudioManager.STREAM_MUSIC, progress, 0);
                        }
                    });

            mRingerVolume
                    .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        public void onStopTrackingTouch(SeekBar arg0) {
                        }

                        public void onStartTrackingTouch(SeekBar arg0) {
                        }

                        public void onProgressChanged(SeekBar arg0,
                                                      int progress, boolean arg2) {
                            audioManager.setStreamVolume(
                                    AudioManager.STREAM_RING, progress, 0);
                        }
                    });

            mAlarmVolume
                    .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        public void onStopTrackingTouch(SeekBar arg0) {
                        }

                        public void onStartTrackingTouch(SeekBar arg0) {
                        }

                        public void onProgressChanged(SeekBar arg0,
                                                      int progress, boolean arg2) {
                            audioManager.setStreamVolume(
                                    AudioManager.STREAM_ALARM, progress, 0);
                        }
                    });

            mNotificationVolume
                    .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        public void onStopTrackingTouch(SeekBar arg0) {
                        }

                        public void onStartTrackingTouch(SeekBar arg0) {
                        }
                        public void onProgressChanged(SeekBar arg0,
                                                      int progress, boolean arg2) {
                            audioManager.setStreamVolume(
                                    AudioManager.STREAM_NOTIFICATION, progress,0);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
